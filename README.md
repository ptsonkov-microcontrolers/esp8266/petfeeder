  # YoFeed
  This is project to create (at di last!) automated feeder for my lovely cat Miroslav (Miro).

  Feeder is designed to work as standalone device, but since it using NodeMCU-ESP8266, there is option to integrate it with HomeAssistant, where you can monitor feeder status, food weight and to trigger manual feeding as well. It support:
  - automaded food weight measuring and feeding on given time interval (configured in code)
  - manual feeding with __FEED__ button
  - manual feeding with HomeAssistant configured button (if integrated)
  - remote feeder monitoring in HomeAssistant/Grafana (if integrated)
  - using NodeRed flow to check feeder connectivity (if integrated)

__Note:__ Integration to HomeAssistant requires:
  - Mosquitto
  - Grafana
  - NodeRed
  - Influx

  This project was created with VSCode+Platformio, but ArduinoIDE can be used as well - not sure how it will behave with includes in this case.

  Feeder is designed to work with A4988 stepper driver, but in the future I'll test it with TMC2208. Code is ready but was not fully tested. TMC2208 is really sensitive to over-voltage and over-current and after 3-4 damaged drivers I decide to stop tests for now. Will continue maybe in the future. You can find wiring helper in _helpers_ directory

  ## Bill of materials (BOM)
  - NodeMCU-ESP8266 - 1 pcs.
  - A4988 (or TMC2208) - 1 pcs.
  - stepper motor Nema-17 42mm (or compatible) - 1 pcs.
  - tactile button 12mm with cap - 2 pcs.
  - load cell 1kg with ADC HX711 - 1 pcs.
  - resistor 10k ohm - 1 pcs.
  - mini piezo buzzer - 1 pcs.
  - LED power supply 12V 2A - 1 pcs.
  - step-down convertor MP1584EN 12V->5V - 1 pcs.
  - wires, soldering iron, soldering wire and so on...

  ## Wiring
  Check code for NodeMCU wiring and _helpers_ directory for stepper driver wiring. Will update with schema and pictures when available

  ## Configuration
  All configurations for the code are placed into _include_ directory. By default you will need to edit something only here and will not need to touch base code in _src_ directory.

  ### Sensitive data (include/code_secrets.h)
  In _examples_ folder you can find template (_code_secrets-example.h_). Move _examples/code_secrets-example.h_ to _include/code_secrets.h_ and set correct sensitive values.

  ### Configuration file (include/code_configs.h)
  Feeder specific parameters can be set into this file. Please read code comments for more details.

  ### Network integration (home_assistant directory)
  If you decide to integrate feeder with your HomeAssistant, in _home_assistant_ directory you can find some useful files:
  - configuration.yaml - examples for integration with HomeAssistant and Mosquitto MQTT broker (add-on). Add this content to your HomeAssistant _/config/configuration.yaml_ file
  - node-red_flow.json - (requires Node-Red addon installed) Node-Red flow to monitor if feeder is alive or not. This flow can be imported into Node-Red and work as follow: 
    - start watching for new messages in _feeder/up_ topic, and send initial _true_ (consider feeder is alive) to topic _feeder/connection_
    - if in waiting period (default 60 seconds) do not receive new message, send _false_ (consider feeder is dead) to topic _feeder/connection_ 
  - grafana_dashboard.json - if you are using HomeAddidtant Influx/Grafana add-ons, you can import this dashboard for feeder monitoring
  
  ### Loadcell calibration
  Calculate loadcell scale to make it measure the weight properly. Follow the procedure:
  - Prepare oblect with known weight (can use kitchen scale to check it)
  - Upload loadcell_scale_calculate.cpp to device and monitor Platformio/Arduino console
  - When asked place a known weight on the loadcell
  - Monitor console output, wait to collect 10-20 measured values, then calculate average value
  - Divide the average value to your known weight
  - Adjust the result to LOADCELL_SCALE in _code_configs.h_

  ### Feeder calibration
  To set tare of the feeder use following procedure
  - Press __RESET__ button then press __FEED__ button within 2 sec (or power cycle with pressed FEED button) and wait to hear 3 short beeps
  - Release FEED button
  - Wait to finish calibration, will hear 1 long beep
  - Feeder will reboot automatically
  - Wait to hear 1 short and 1 longer beeps.
  Feeder is ready and will start measuring the food weight and autofeeding

  ## Feeder problems
  When there is problem with the feeder (stuck granule, empty container or something else), feeder will retry to reach target weight and then will stop, you will hear 1 long and 3 short low beeps. In order to prevent stepper driver and/or stepper motor, feeder will remain at this state and continue to beeping as described untill you check/clean it and reset it, or refill and push __FEED__ button (push feed from HomeAssistant also work if available). This will restore feeder back to normal operation.
