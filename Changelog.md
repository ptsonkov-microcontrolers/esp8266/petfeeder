# Changelog

## v1.0
Initial release. Full describtion, wiring and other details are in README.md

## v2.0
- Entirely new design
- Replate stepper driver from A4988 to DRV8825
- Use microstepping to reduce noise
  - Update code to fit new design with gears and rubber valve

## v2.1
- Update code_config - change input parameters for valve
- Update main application code with 
  - new valve rotation calculation
  - new manual feeding function
  - new reboot function
- Rename and reorganize functions
- Update Lovelace panel and Grafana dashboard
- Remove Grafana dashboard an Influx related config. Use Lovelace panel instead 