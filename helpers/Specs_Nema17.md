# Nema17 Stepper Motor Specifications

## 42SHDC3025-24B
https://www.yoycart.com/Product/562154684157  
https://caggius.wordpress.com/anet-a8-hardware-specifications  

```
Rated Power             14W
Rated Voltage           3.96V
Rated Current           0.9A
Rated Speed             1000rpm
Rated Torque            0.34NM
Holding Torque          0.4N.M
Step Angle              1.8°
Step Angle Accuracy     ±5%
Phase                   2
Resistance              4.4Ω±10%
Temperature Rise        80K Max
Ambient Temperature     -20℃~+50℃
Ambient Humidity        90% Max
Insulation Resistance   100 MΩ Min. ,500VDC
Size                    42*42*40(mm)
Weight                  280g
```

## 17HS4401S-22B
https://www.aliexpress.com/item/32829022738.html  
https://mechblock.in/p/nema17-stepper-motor-17hs4401s/?srsltid=AfmBOorHLU4pb1EXoIaZed-YDNPO4v1tK1N8WSMNOoLBiswn9uuzji5Y  
```
Rated Voltage           3.6V
Rated Current           1.5A/Phase
Phase Resistance(20℃)  2.4*(1±15%)Ω/Phase
Holding Torque          ≥420mN.m
Maximum slewing rate    ≥1900 PPS
Electrical strength     AC600V/1mA/1S
Step Angle(degrees)     1.8°±0.09°
Phase                   2
Phase Inductance        3.7*(1±20%)mH/Phase
Positioning Torque      15 mN.m    REF.
Maximum starting rate   ≥1500 PPS
Insulation resistance   ≥100 MΩ (DC 500V)
Mass: 280g
```