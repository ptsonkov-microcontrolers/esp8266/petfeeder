# Stepper drivers wiring

x. Set stepper driver Vref
connect stepper driver STEP, DIR, VDD, GND (MOTOR AND MOTOR POWER SHOULD NOT BE CONNECTED!)
- for A4988 - Measure with multimeter between potentiometer and GND
- for TMC2208 - Measure with multimeter between potentiometer and hole next to GND

x. Motor pinout
- Look motor with connector in front and rotor pointing up
- Wires count from left to right
- Coil1 - Green/Black
- Coil2 - Red/Blue
```
+-------+-------+-------+-------+-------+-------+
|  Red  | empty | Green |  Blue | empty | Black |
+-------+-------+-------+-------+-------+-------+
|   2B  |   -   |   1B  |   2A  |   -   |   1A  | -> to stepper driver (common)
+-------+-------+-------+-------+-------+-------+
|  OA2  |   -   |  OB2  |  OA1  |   -   |  OB1  | -> to stepper driver (BigThreeTech)
+-------+-------+-------+-------+-------+-------+
|  M1B  |   -   |  M2B  |  M1A  |   -   |  M2A  | -> to stepper driver (white one)
+-------+-------+-------+-------+-------+-------+
|   B-  |   -   |   A-  |   B+  |   -   |   A+  | -> to RAMPS board (to be confirmed!)
+-------+-------+-------+-------+-------+-------+
```

x. Vref online calculator
https://mak3r.de/2020/04/03/stepper-driver-current-and-vref-calculator/


## Motor connection to A4988

x. Default steps per revolution
- full step - 200 steps per revolution : 360° / 1.8° = 200

x. Calculate Vref for A4988
Vref = 8 * [Motor rated Current] * [Driver sense resistor]
- example with sense resistor R100 (check on dive board, two equal resistors, depend on manifacturer)
Vref = 8 * 0.9 * 0.1 = 0.720V
- safety Vref
Vref = 0.720 * 90% = 0.648V

x. Set stepper driver Vref (specific for A4988)
- connect stepper driver STEP, DIR, VDD, VMOT, GND (MOTOR SHOULD NOT BE CONNECTED!)
- measure with multimeter between potentiometer and GND

x. Pinout table matching A4988 with Nema17 wires and Arduino Nano
```
                     +---------------------------+
                     |                           |
                  ---+ ENABLE               VMOT +---\
                     |                           |     Motor power 9V-36V
                  ---+ MS1                   GND +---/
                     |                           |
                  ---+ MS2                    2B +--- Red
                     |                           |
                  ---+ MS3                    2A +--- Blue
                     |                           |
                 +---+ RESET                  1A +--- Green
                 |   |                           |
                 +---+ SLEEP                  1B +--- Black
                     |                           |
    Arduino pin 3 ---+ STEP                  VDD +--- Arduino pin 5V
                     |                           |
    Arduino pin 2 ---+ DIRECTION     +-+     GND +--- Arduino pin GND
                     |               +-+         |
                     +---------------------------+
```


## Motor connection to DRV8825

x. Default steps per revolution
- full step - 200 steps per revolution : 360° / 1.8° = 200

x. Calculate Vref for DRV8825
Vref =[Motor rated Current] / 2
- example with sense resistor R100 (check on dive board, two equal resistors, depend on manifacturer)
Vref = 0.9 / 2 = 0.45V
- safety Vref
Vref = 0.45 * 90% = 0.405V

x. Set stepper driver Vref (specific for DRV8825)
- connect stepper driver STEP, DIR, FLT, UMOT, GND (MOTOR SHOULD NOT BE CONNECTED!)
- measure with multimeter between potentiometer and GND

x. Pinout table matching A4988 with Nema17 wires and Arduino Nano
```
                     +---------------------------+
                     |                           |
                  ---+ ENABLE +-+           UMOT +---\
                     |        +-+                |     Motor power 8.2V-45V
                  ---+ MS0                   GND +---/
                     |                           |
                  ---+ MS1                    B2 +--- Red
                     |                           |
                  ---+ MS2                    B1 +--- Blue
                     |                           |
  Arduino pin 5V +---+ RESET                  A1 +--- Green
                 |   |                           |
                 +---+ SLEEP                  A2 +--- Black
                     |                           |
    Arduino pin 3 ---+ STEP                  FLT +--- Arduino pin 5V* (see below!) 
                     |                           |
    Arduino pin 2 ---+ DIRECTION             GND +--- Arduino pin GND
                     |                           |
                     +---------------------------+
```

* it is safe to connect the FAULT pin directly to a logic supply (there is a 1.5k resistor 
between the IC output and the pin to protect it), so the DRV8825 module can be used in systems 
designed for the A4988 that route logic power to this pin.


## Motor connection to TMC2208 BigTreeTech v3.0 or Trinamic

x. Default steps per revolution
- 1/8 step - 1600 steps per revolution : 360° / (1.8° / 8) = 1600

x. Calculate Vref for TMC2208
Vref = ([Motor rated Current] * ([Driver sense resistor] + 0.02Ω) * 2.5V) / 0.325V
- example with sense resistor R110 (check on dive board, two equal resistors, depend on manifacturer)
Vref = (0.9 * (0.11 + 0.02) * 2.5) / 0.325 = 0.9V
- safety Vref
Vref = 0.9 * 90% = 0.81V

x. Set stepper driver Vref (specific for TMC2208)
- connect stepper driver STEP, DIR, VDD, GND (MOTOR SHOULD NOT BE CONNECTED!)
- measure with multimeter between potentiometer and hole next to GND

x. Pinout table matching TMC2208 with Nema17 wires
```
                     +---------------------------+
                     |                           |
  Arduino pin GND ---+ ENABLE  o o    +-+   VMOT +---\
                     |          o     +-+        |     Motor power 9V-25V
                  ---+ MS1                   GND +---/
                     |                           |
                  ---+ MS2               M1B/OA2 +--- Red
                     |                           |
                  ---+ PDN/NC            M1A/OA1 +--- Blue
                     |                           |
                  ---+ PDN               M2A/OB1 +--- Green
                     |                           |
                  ---+ CLK               M2B/OB2 +--- Black
                     |                           |
    Arduino pin 3 ---+ STEP                  VDD +--- Arduino pin 5V
                     |                           |
    Arduino pin 2 ---+ DIRECTION             GND +--- Arduino pin GND
                     |                           |
                     +---------------------------+
```