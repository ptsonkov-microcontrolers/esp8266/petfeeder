#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <AccelStepper.h>
#include <Bounce2.h>
#include <HX711.h>
#include <EEPROM.h>
#include "tones.h"
#include "code_secrets.h"
#include "code_configs.h"

// ============================================================================
// === Controller pins definition
#define MOTOR_INTERFACE 1
#define DRIVER_STEP_PIN D1
#define DRIVER_DIRECTION_PIN D2
#define DRIVER_ENABLE_PIN D3
#define LOADCELL_DOUT_PIN D6
#define LOADCELL_SCK_PIN D7
#define BUTTON_FEED_PIN D4
#define BUZZER_PIN D5

// ============================================================================
// === Stepper motor

// Set stepper driver parameters. Change with caution!
// Microsteps are defined with jumper setting (check configuration for each drive!)
// Valid microsteps:
//  1 (full step)
//  2 (1/2 step)
//  4 (1/4 step)
//  8 (1/8 step)
// 16 (1/16 step)
// 32 (1/32 step)
#define MOTOR_MAX_SPEED 600
#define MOTOR_ACCELERATION 500
#define MOTOR_FULL_STEPS_PER_REVOLUTION 200
#define MOTOR_DRIVER_MICROSTEPS 16
#define MOTOR_DEGREES_PER_STEP 360.0 / (MOTOR_FULL_STEPS_PER_REVOLUTION * MOTOR_DRIVER_MICROSTEPS)

// ============================================================================
// === Create instances
AccelStepper motor(MOTOR_INTERFACE, DRIVER_STEP_PIN, DRIVER_DIRECTION_PIN);
Bounce2::Button feedButton = Bounce2::Button();
HX711 loadcell;
WiFiClient espClient;
PubSubClient mqttClient(espClient);

// ============================================================================
// === Set WiFi credentials
const char *wifi_ssid = SECRET_WIFI_SSID;
const char *wifi_pass = SECRET_WIFI_PASS;

// ============================================================================
// === Set MQTT endpoint and topics
const char *mqtt_server = SECRET_MQTT_SERVER;
const int mqtt_port = SECRET_MQTT_PORT;
const char *mqtt_user = SECRET_MQTT_USER;
const char *mqtt_pass = SECRET_MQTT_PASS;
const char *mqtt_topic_out_feederror = "feeder/feederror";
const char *mqtt_topic_out_weight = "feeder/weight";
const char *mqtt_topic_out_live = "feeder/up";
const char *mqtt_topic_in_cmd = "feeder/button";

// ============================================================================
// === Variables
unsigned int bootButtonTimeout = 2000;
bool feedError = false;
int foodCurrentWeightGrams;
unsigned long weightCheckTimeNow, weightCheckTimeDelta;
unsigned long feedTimeNow, feedTimeDelta;
unsigned long livenessTimeNow, livenessTimeDelta;
float valveGearRatio = 3.1;
float valveMoveDegrees = 360 / VALVE_SEGMENTS;
float motorMoveForwardSteps = (valveMoveDegrees / (MOTOR_DEGREES_PER_STEP)) * valveGearRatio;
#if MOVE_VALVE_BACKWARDS == true
float motorMoveBackwardSteps = ((valveMoveDegrees / 30) / (MOTOR_DEGREES_PER_STEP)) * valveGearRatio;
#elif MOVE_VALVE_BACKWARDS == false
float motorMoveBackwardSteps = 0;
#endif
int soundSpeed = 1000;
int pauseCoef = 1;
int calibrationStartToneSeq[3] = {_C5, _C5, _C5};
int calibrationStartToneLen[3] = {4, 4, 4};
int calibrationStopToneSeq[1] = {_C5};
int calibrationStopToneLen[1] = {1};
int normalStartToneSeq[2] = {_G4, _C5};
int normalStartToneLen[2] = {4, 2};
int containerEmptyToneSeq[4] = {_C4, _C4, _C4, _C4};
int containerEmptyToneLen[4] = {1, 4, 4, 4};
int eepromAddr = 0;

// ============================================================================
// === Forward function declaration
void setupWiFi();
void mqttCallback(char *topic, byte *payload, unsigned int length);
void mqttReconnect();
bool bootButton();
void plateCalibrate();
void measureFood();
void rotateValve(int moveCount);
void feedAuto();
void feedManual();
void playSound(int *sequence, int *duration, int size);

void setup()
{
  Serial.begin(115200);

  // Initially disable driver
  // disabled = HIGH
  // enabled = LOW (default)
  pinMode(DRIVER_ENABLE_PIN, OUTPUT);
  digitalWrite(DRIVER_ENABLE_PIN, HIGH);

  // Set max speed in steps/sec
  // Set ENABLE pin, set inverse ENABLE pin
  motor.setMaxSpeed(MOTOR_MAX_SPEED);
  motor.setAcceleration(MOTOR_ACCELERATION);

  // Set loadcell parameters
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  // Configure button pin, debounce time in miliseconds and pressed state
  feedButton.attach(BUTTON_FEED_PIN, INPUT);
  feedButton.interval(25);
  feedButton.setPressedState(HIGH);

  // Calibration of the feeder or run normal
  delay(2000);
  if (bootButton() == true)
  {
    Serial.println("========================================");
    Serial.println("=== CALIBRATING...");
    playSound(calibrationStartToneSeq, calibrationStartToneLen, 3);
    plateCalibrate();
    Serial.println("Calibration done. Restarting.");
    playSound(calibrationStopToneSeq, calibrationStopToneLen, 1);
    ESP.reset();
  }
  else
  {
    playSound(normalStartToneSeq, normalStartToneLen, 2);

    long readTare = 0;
    EEPROM.begin(512);
    EEPROM.get(eepromAddr, readTare);
    EEPROM.end();
    loadcell.set_scale(LOADCELL_SCALE);
    loadcell.set_offset(readTare);

    if (USE_HOME_ASSISTANT)
    {
      setupWiFi();

      // Set MQTT server and callback listener (for incomming messages)
      mqttClient.setServer(mqtt_server, mqtt_port);
      mqttClient.setCallback(mqttCallback);

      // Startup measure and feeding
      measureFood();
      feedAuto();

      // Startup liveness probe
      mqttClient.publish(mqtt_topic_out_live, String("ping").c_str(), true);
    }
    else
    {
      // Startup measure and feeding
      measureFood();
      feedAuto();
    }

    Serial.println("========================================");
    Serial.println("=== Start regular food checks");
  }
}

void loop()
{
  // Set delta time
  feedTimeDelta = millis() / 1000;
  livenessTimeDelta = millis() / 1000;
  weightCheckTimeDelta = millis() / 1000;

  if (USE_HOME_ASSISTANT)
  {
    if (!mqttClient.connected())
    {
      mqttReconnect();
    }

    // Watch for incommint MQTT messages
    mqttClient.loop();

    if (livenessTimeDelta - livenessTimeNow >= LIVENESS_PROBE_TIME_SECONDS)
    {
      Serial.println(">>> Publish liveness probe");
      mqttClient.publish(mqtt_topic_out_live, String("ping").c_str(), true);
      livenessTimeNow = millis() / 1000;
    }

    if (weightCheckTimeDelta - weightCheckTimeNow >= FOOD_WEIGHT_CHECK_SECONDS)
    {
      measureFood();
      weightCheckTimeNow = millis() / 1000;
    }
  }

  // Watch for button state changes
  feedButton.update();

  if (feedButton.pressed())
  {
    Serial.println("Manual feeding with button");
    feedManual();
  }

  if (feedTimeDelta - feedTimeNow >= FEED_TIME_INTERVAL_SECONDS)
  {
    if (USE_HOME_ASSISTANT)
    {
      Serial.println(">>> Publish feeder error");
      mqttClient.publish(mqtt_topic_out_feederror, String(feedError).c_str(), true);
    }
    feedAuto();
  }
}

void setupWiFi()
{
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void mqttCallback(char *topic, byte *payload, unsigned int length)
{
  payload[length] = '\0';
  Serial.print("Message received on topic (");
  Serial.print(topic);
  Serial.print("): ");
  Serial.println((char *)payload);

  if (strcmp((char *)payload, "feed") == 0)
  {
    Serial.println("Manual feeding with WiFi/MQTT");
    feedManual();
  }
  else if (strcmp((char *)payload, "reboot") == 0)
  {
    Serial.println("Reboot with WiFi/MQTT");
    ESP.restart();
  }
}

void mqttReconnect()
{
  while (!mqttClient.connected())
  {
    Serial.print("Attempting MQTT connection: ");
    if (mqttClient.connect("petFeeder", mqtt_user, mqtt_pass))
    {
      Serial.println("connected");
      mqttClient.subscribe(mqtt_topic_in_cmd);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

bool bootButton()
{
  unsigned long startTime = millis();
  unsigned long heldTime = 0;
  while (digitalRead(BUTTON_FEED_PIN) == HIGH)
  {
    heldTime = millis() - startTime;
    if (heldTime > bootButtonTimeout)
    {
      return true;
    }
  }
  return false;
}

void plateCalibrate()
{
  long readTare;
  Serial.print("Offset value (EEPROM):     ");
  EEPROM.begin(512);
  EEPROM.get(eepromAddr, readTare);
  EEPROM.end();
  Serial.println(readTare);
  delay(1000);
  Serial.println("Set empty scale");
  loadcell.set_scale();
  delay(1000);
  Serial.print("Offset raw:                ");
  Serial.println(loadcell.get_offset());
  delay(1000);
  Serial.println("Reset tare");
  loadcell.tare();
  delay(1000);
  Serial.print("Offset new:                ");
  long newOffset = loadcell.get_offset();
  Serial.println(newOffset);

  Serial.println("Write new offset to EEPROM...");
  EEPROM.begin(512);
  EEPROM.put(eepromAddr, newOffset);
  EEPROM.commit();
  EEPROM.end();
  delay(1000);

  Serial.print("Current EEPROM value:      ");
  EEPROM.begin(512);
  EEPROM.get(eepromAddr, readTare);
  EEPROM.end();
  Serial.println(readTare);
  delay(1000);
}

void measureFood()
{
  loadcell.power_up();

  foodCurrentWeightGrams = int(loadcell.get_units(10));

  if (USE_HOME_ASSISTANT)
  {
    Serial.println(">>> Publish current food weight");
    mqttClient.publish(mqtt_topic_out_weight, String(foodCurrentWeightGrams).c_str(), true);
  }

  Serial.print(foodCurrentWeightGrams);
  loadcell.power_down();
}

void rotateValve(int moveCount)
{
  digitalWrite(DRIVER_ENABLE_PIN, LOW);
  for (int i = 0; i < moveCount; i++)
  {
    motor.setCurrentPosition(0);
    motor.move(motorMoveForwardSteps);
    motor.runToPosition();
    motor.setCurrentPosition(0);
    motor.move(-motorMoveBackwardSteps);
    motor.runToPosition();
  }
  digitalWrite(DRIVER_ENABLE_PIN, HIGH);
}

void feedAuto()
{
  if (feedError)
  {
    Serial.println("Feeder problem or empty container. Check and push feed button.");
    playSound(containerEmptyToneSeq, containerEmptyToneLen, 4);
    feedTimeNow = millis() / 1000;
  }
  else
  {
    int rotateAgainCount = 0;
    while (foodCurrentWeightGrams < FOOD_MINIMUM_WEIGHT_GRAMS)
    {
      if (rotateAgainCount >= ROTATE_AGAIN_LIMIT)
      {
        Serial.println("g \t | Feeding retry reached");
        feedError = true;
        return;
      }
      else
      {
        Serial.println("g \t | Food not enough. Rotate feeder");
        rotateValve(ROTATE_WITH_VALVE_SEGMENTS);
        measureFood();
        rotateAgainCount++;
      }
    }
    Serial.println("g \t | Food enough. Wait next check");
    feedTimeNow = millis() / 1000;
  }
}

void feedManual()
{
  if (foodCurrentWeightGrams < FOOD_MAXIMUM_WEIGHT_GRAMS)
  {
    rotateValve(ROTATE_WITH_VALVE_SEGMENTS);
    measureFood();
  }
}

void playSound(int *sequence, int *duration, int size)
{
  int soundSpeed = 1000;
  int soundPauseCoef = 1;
  for (int toneNum = 0; toneNum < size; toneNum++)
  {
    int toneLen = soundSpeed / duration[toneNum];
    tone(BUZZER_PIN, sequence[toneNum], toneLen);
    int pauseBetweenTones = toneLen * soundPauseCoef;
    delay(pauseBetweenTones);
    noTone(BUZZER_PIN);
  }
}
