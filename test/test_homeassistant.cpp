#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include "code_secrets.h"

// Set WiFi credentials
const char *wifi_ssid = SECRET_WIFI_SSID;
const char *wifi_pass = SECRET_WIFI_PASS;

// Set MQTT endpoint
const char *mqtt_server = SECRET_MQTT_SERVER;
const int mqtt_port = SECRET_MQTT_PORT;
const char *mqtt_user = SECRET_MQTT_USER;
const char *mqtt_pass = SECRET_MQTT_PASS;

// Set MQTT topics
#define mqtt_topic_out_weight "feeder/weight"
#define mqtt_topic_in_cmd "feeder/button"

const int ledPin = D1;
const int buttonPin = D2;
unsigned long FeedTimeNow = 0;
unsigned long FeedTimeDelta = 0;

WiFiClient espClient;
PubSubClient mqttClient(espClient);

// Forward function declaration
void setup_wifi();
void callback(char *topic, byte *payload, unsigned int length);
void reconnect();

void setup()
{
  Serial.begin(115200);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  setup_wifi();

  mqttClient.setServer(mqtt_server, mqtt_port);
  mqttClient.setCallback(callback);
}

void loop()
{
  if (!mqttClient.connected())
  {
    reconnect();
  }
  mqttClient.loop();

  FeedTimeDelta = millis() / 1000;
  if (FeedTimeDelta - FeedTimeNow >= 10)
  {
    Serial.println("publish weight");
    mqttClient.publish(mqtt_topic_out_weight, "27");
    FeedTimeNow = millis() / 1000;
  }
}

void setup_wifi()
{
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message received [");
  Serial.print(topic);
  Serial.print("] ");
  for (unsigned int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  if (strcmp((char*)payload, "PRESS") == 0)
  {
    Serial.println("Run feeder motor");
  }
}

void reconnect()
{
  while (!mqttClient.connected())
  {
    Serial.print("Attempting MQTT connection...");
    if (mqttClient.connect("nodemcuClient", mqtt_user, mqtt_pass))
    {
      Serial.println("connected");
      mqttClient.subscribe(mqtt_topic_in_cmd);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}
