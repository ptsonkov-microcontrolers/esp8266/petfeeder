#include <Arduino.h>
#include "tones.h"

int soundPin = 8;

// tone sequences and tone lengths
int sound1ToneSeq[3] = {_C5, _C5, _C5};
int sound1ToneLen[3] = {4, 4, 4};
int sound2ToneSeq[1] = {_C5};
int sound2ToneLen[1] = {1};

void play(int *sequence, int *duration, int size);

void setup()
{
    Serial.begin(115200);
    play(sound1ToneSeq, sound1ToneLen, 3);
    delay(2000);
    play(sound2ToneSeq, sound2ToneLen, 1);
}

void loop() {}

void play(int *sequence, int *duration, int size)
{
    int soundSpeed = 1000;
    int soundPauseCoef = 1;
    for (int toneNum = 0; toneNum < size; toneNum++)
    {
        int toneLen = soundSpeed / duration[toneNum];
        tone(soundPin, sequence[toneNum], toneLen);
        int pauseBetweenTones = toneLen * soundPauseCoef;
        delay(pauseBetweenTones);
        noTone(soundPin);
    }
}
