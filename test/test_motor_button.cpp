#include <Arduino.h>
#include <AccelStepper.h>
#include <Bounce2.h>

// Define the controller pins
#define MOTOR_INTERFACE 1
#define MOTOR_FULL_STEPS_PER_REVOLUTION 200
#define MOTOR_DRIVER_MICROSTEPS 16
#define MOTOR_DEGREES_PER_STEP 360.0 / (MOTOR_FULL_STEPS_PER_REVOLUTION * MOTOR_DRIVER_MICROSTEPS)
#define DRIVER_STEP_PIN D1
#define DRIVER_DIRECTION_PIN D2
#define DRIVER_ENABLE_PIN D3
#define BUTTON_FEED_PIN D4

// Create instances
AccelStepper motor(MOTOR_INTERFACE, DRIVER_STEP_PIN, DRIVER_DIRECTION_PIN);
Bounce2::Button feedButton = Bounce2::Button();

int direction = 1;
float moveDegrees = 90;
float moveSteps = moveDegrees / (MOTOR_DEGREES_PER_STEP);

void setup()
{
    Serial.begin(115200);
    // Serial.println("=============================");
    // Serial.println(MOTOR_DEGREES_PER_STEP);
    // Serial.println(moveDegrees);
    // Serial.println(moveSteps);
    // Serial.println("=============================");
    // Serial.println(moveSteps * direction);
    // direction = -direction;
    // Serial.println(moveSteps * direction);
    // Serial.println("=============================");

    pinMode(DRIVER_ENABLE_PIN, OUTPUT);
    digitalWrite(DRIVER_ENABLE_PIN, HIGH);

    motor.setMaxSpeed(50);
    motor.setAcceleration(30);

    // Configure button pin, debounce time in miliseconds and pressed state
    feedButton.attach(BUTTON_FEED_PIN, INPUT_PULLUP);
    feedButton.interval(25);
    feedButton.setPressedState(LOW);
}

void loop()
{
    feedButton.update();

    if (feedButton.pressed())
    {
        Serial.print("move ");
        Serial.println((direction == 1) ? "FORWARD..." : "BACKWARD...");

        digitalWrite(DRIVER_ENABLE_PIN, LOW);
        motor.setCurrentPosition(0);
        motor.move(moveSteps * direction);
        motor.runToPosition();
        digitalWrite(DRIVER_ENABLE_PIN, HIGH);

        Serial.print("movement ");
        Serial.println((direction == 1) ? "FORWARD completed" : "BACKWARD completed");

        // Invert the direction of the stepper
        direction = -direction;
    }
}