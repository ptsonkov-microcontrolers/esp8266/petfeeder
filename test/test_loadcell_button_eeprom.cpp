#include <Arduino.h>
#include <HX711.h>
#include <Bounce2.h>
#include <EEPROM.h>

// Pins definition
#define LOADCELL_DOUT_PIN D6
#define LOADCELL_SCK_PIN D7
#define BUTTON_CALIBRATE_PIN D4

// Create instances
HX711 loadcell;
Bounce2::Button calibrateButton = Bounce2::Button();

// Variables
unsigned long bootButtonTimeout = 2000;
unsigned long readingTimeout = 2;
unsigned long readingTimeNow = 0;
unsigned long readingTimeDelta = 0;
float weightScale = 2169.0;
int eepromAddr = 0;

// Forward function declaration
bool bootButton();

void setup()
{
    Serial.begin(115200);

    // Set loadcell parameters
    loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

    // no need from "pinMode(BUTTON_CALIBRATE_PIN, INPUT)"
    // but can use manually "digitalRead(BUTTON_CALIBRATE_PIN) == HIGH"
    calibrateButton.attach(BUTTON_CALIBRATE_PIN, INPUT);
    calibrateButton.interval(25);
    calibrateButton.setPressedState(HIGH);

    delay(2000);

    if (bootButton() == true)
    {
        long readTare;

        Serial.println("========================================");
        Serial.println("=== CALIBRATING...");
        Serial.print("Offset value (EEPROM):     ");
        EEPROM.begin(512);
        EEPROM.get(eepromAddr, readTare);
        EEPROM.end();
        Serial.println(readTare);
        delay(1000);
        Serial.println("Set empty scale");
        loadcell.set_scale();
        delay(1000);
        Serial.print("Offset raw:                ");
        Serial.println(loadcell.get_offset());
        delay(1000);
        Serial.println("Reset tare");
        loadcell.tare();
        delay(1000);
        Serial.print("Offset new:                ");
        long newOffset = loadcell.get_offset();
        Serial.println(newOffset);

        Serial.println("Write new offset to EEPROM...");
        EEPROM.begin(512);
        EEPROM.put(eepromAddr, newOffset);
        EEPROM.commit();
        EEPROM.end();
        delay(1000);

        Serial.print("Current EEPROM value:      ");
        EEPROM.begin(512);
        EEPROM.get(eepromAddr, readTare);
        EEPROM.end();
        Serial.println(readTare);
        delay(1000);

        Serial.println("Calibration done. Restarting.");
        delay(3000);
        ESP.reset();
    }
    else
    {
        long readTare;

        Serial.println("========================================");
        Serial.println("=== Before setting up the scale:");
        // print a raw reading from the ADC
        Serial.print("read:             ");
        Serial.println(loadcell.read());
        // print the average of 20 readings from the ADC
        Serial.print("read average:     ");
        Serial.println(loadcell.read_average(20));
        // print the average of 5 readings from the ADC minus the tare weight (not set yet)
        Serial.print("get value:        ");
        Serial.println(loadcell.get_value(5));
        // print the average of 5 readings from the ADC minus tare weight (not set) divided
        // by the SCALE parameter (not set yet)
        Serial.print("get units:        ");
        Serial.println(loadcell.get_units(5), 1);

        // Set conversion scale
        // this value is obtained by calibrating the scale with known weights
        // How to calibrate your load cell (https://github.com/bogde/HX711)
        // 1. Call set_scale() with no parameter.
        // 2. Call tare() with no parameter.
        // 3. Place a known weight on the scale and call get_units(10).
        // 4. Divide the result in step 3 to your known weight. You should get about the parameter you need to pass to set_scale().
        // 5. Adjust the parameter in step 4 until you get an accurate reading.
        loadcell.set_scale(weightScale);
        // // reset the scale to 0
        // loadcell.tare();
        EEPROM.begin(512);
        EEPROM.get(eepromAddr, readTare);
        EEPROM.end();
        loadcell.set_offset(readTare);

        Serial.println("========================================");
        Serial.println("=== After setting up the scale:");
        // print a raw reading from the ADC
        Serial.print("read:             ");
        Serial.println(loadcell.read());
        // print the average of 10 readings from the ADC
        Serial.print("read average:     ");
        Serial.println(loadcell.read_average(10));
        // print the average of 5 readings from the ADC minus the tare weight, set with tare()
        Serial.print("get value:        ");
        Serial.println(loadcell.get_value(5));
        // print the average of 5 readings from the ADC minus tare weight, divided
        // by the SCALE parameter set with set_scale
        Serial.print("get units:        ");
        Serial.println(loadcell.get_units(5), 1);

        Serial.println("========================================");
        Serial.println("=== Readings:");
        readingTimeNow = millis() / 1000;
    }
}

void loop()
{
    readingTimeDelta = millis() / 1000;
    calibrateButton.update();

    if (calibrateButton.pressed())
    {
        Serial.println(">>> do something >>>");
    }

    if (readingTimeDelta - readingTimeNow >= readingTimeout)
    {
        // wake up ADC from sleep mode
        loadcell.power_up();
        Serial.print("one reading:   ");
        Serial.print(loadcell.get_units(), 1);
        Serial.print("\t| average:   ");
        Serial.println(loadcell.get_units(10), 1);
        // put the ADC in sleep mode
        loadcell.power_down();
        readingTimeNow = millis() / 1000;
    }
}

bool bootButton()
{
    unsigned long startTime = millis();
    unsigned long heldTime = 0;
    while (digitalRead(BUTTON_CALIBRATE_PIN) == HIGH)
    {
        heldTime = millis() - startTime;
        if (heldTime > bootButtonTimeout)
        {
            return true;
        }
    }
    return false;
}
