// Enable or disable integration with WiFi and HomeAssistant
#define USE_HOME_ASSISTANT true

// Period to send "I'm live" messages to MQTT
#define LIVENESS_PROBE_TIME_SECONDS 20

// Loadcell scale value, check README.md for more details how to obtain it
#define LOADCELL_SCALE 2169

// Food weight limits
#define FOOD_MINIMUM_WEIGHT_GRAMS 20
#define FOOD_MAXIMUM_WEIGHT_GRAMS 50

// Feeding time in seconds
// Timer to check food weight and trigger the feeder
// For 80gr food be day for 5kg cat, min 20gr in bowl
// calculated feeding time is 6h:
// 4 * 6 = 24 i.e. 4 feedings * 20gr = 80gr
// 6h * 60min * 60sec = 21600sec to check food weight at next feeding
#define FOOD_WEIGHT_CHECK_SECONDS 60
#define FEED_TIME_INTERVAL_SECONDS 21600

// Count of valve segments
// How granule spaces are there per one full valve rotation
#define VALVE_SEGMENTS 6

// Feeding rotation sequence count (forward-backward)
// example: count 2 will move forward->backward->forward->backward
#define ROTATE_WITH_VALVE_SEGMENTS 1

// Move backward to unblock
// Set to true if valve is solid (not rubber)
// This can unblock valve from stuck granules
#define MOVE_VALVE_BACKWARDS false

// Counter of retrys before feeder to fall in fail state. 
// Do not set it too high, 4/5/6 is resonable value
#define ROTATE_AGAIN_LIMIT 6